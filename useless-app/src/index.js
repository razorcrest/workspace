import React from 'react';
import ReactDOM from 'react-dom';
import HeaderBlock from './components/HeaderBlock';
import './index.css';

const Applist = () => {
  const items = ['Item 1', 'Item 2','Item 3','Item 4'];
  const firstItem = <li> Item 0</li>;

  const isAuth = 0;

  return  (
    <ul>
      { isAuth ? firstItem :null}
      { items.map( item => <li>{item}</li>)}
      <li>{items[0]}</li>
      <li>{items[1]}</li>
    </ul>
  )
}

const AppHeader = () => {
  return  <h1 className="header">You again?</h1>;
}

 const AppInput = () => {
   const placeholder = 'Type text...';

   return (
     <label htmlFor="search">
       <input id="search" placeholder={placeholder} />
     </label>

   )
 }

const App = () =>{
  return (
    <>
      <HeaderBlock />
      <AppHeader />
      <AppInput />
      <Applist />
      <AppHeader />
      <Applist />
    </>
  );
}

ReactDOM.render(<App />, document.getElementById('root')
);
